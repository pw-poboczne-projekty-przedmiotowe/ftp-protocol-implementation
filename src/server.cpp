#include "server.hpp"

#include <iostream>
#include <filesystem>
#include <fstream> 
#include <streambuf>
#include <iostream>

Server::Server(Telnet *control) : control_socket(control){ 
}

Server::~Server(){

}

void Server::waitForConnect(){ 
    control_socket->waitForConnect();
}

std::string Server::receiveMessage(){
    return control_socket->receiveMessage();
}

void  Server::sendFtpResponse(int code, std::string message)
{
    return control_socket->sendFtpResponse(code, message);
}

void Server::handleMsg(std::string message){

    char *token = strtok(&message[0], " ");

    if(strcmp("MKDIR", token) == 0){
        token=strtok(NULL," \n");
            if(mkdir(token, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)<0){
                fprintf(stderr, "cannot create directory %s\n", strerror(errno));
                control_socket->sendFtpResponse(451, "Requested action aborted. Local error in processing.");
            } else {
                control_socket->sendFtpResponse(200, "OK");
            }

    } else if(strcmp("RMDIR", token) == 0){
        token=strtok(NULL," \n");
            if(rmdir(token)<0){
                fprintf(stderr, "cannot remove directory %s\n", strerror(errno));
                control_socket->sendFtpResponse(451, "Requested action aborted. Local error in processing.");
            } else {
                control_socket->sendFtpResponse(200, "OK");
            }
        
    } else if(strcmp("CHANGE", token) == 0){
        token=strtok(NULL," \n");
            if(chdir(token) < 0) {
                fprintf(stderr, "cannot change to directory %s\n", strerror(errno));
                control_socket->sendFtpResponse(451, "Requested action aborted. Local error in processing.");
            } else {
                control_socket->sendFtpResponse(200, "OK");
            }
    } else if(strcmp("LIST", token) == 0){
        control_socket->sendFtpResponse(200, "OK");

        std::stringstream buffer;
        for (const auto & entry : std::filesystem::directory_iterator(std::filesystem::current_path()))
            buffer << entry.path().filename() << "   ";

        control_socket->sendFtpResponse(200, buffer.str());
        

    } else if(strcmp("STORE", token) == 0){
        token=strtok(NULL," \n");
        control_socket->sendFtpResponse(200, "OK");
        usleep(40000);

        std::string str = token;
        std::string temp;

        for(char &c : str)
        {
            if(c == '/')
            {
                temp.clear();
            }
            else
                temp += c;
        }


        data_transmittion.connect("127.0.0.1");

        std::ofstream file (temp);

        if(file.is_open())
        {
            data_transmittion.receiveData();

            file << data_transmittion.getData();

            file.close();
        }

    } else if(strcmp("RETRIEVE", token) == 0){
        token=strtok(NULL," \n");
        if(access( token, F_OK ) != -1)
        {
            std::string str = token;
            std::string temp;

            for(char &c : str)
            {
                if(c == '/')
                {
                    temp.clear();
                }
                else
                    temp += c;
            }

            control_socket->sendFtpResponse(200, "OK");
            usleep(40000);
            std::fstream fs;
            fs.open (temp);
            if(!fs)
                std::cout << "Cannot load a file\n";
            std::stringstream buffer;
            buffer << fs.rdbuf();
            fs.close();

            data_transmittion.connect("127.0.0.1");
            data_transmittion.sendData(buffer.str());
        }
        else
            control_socket->sendFtpResponse(451, "Requested action aborted. Local error in processing.");

    } else if(strcmp("QUIT", token) == 0){
        control_socket->sendFtpResponse(221, "Service closing control connection. ");

    } else {
        control_socket->sendFtpResponse(201, "Command not implemented");
    }

}

bool Server::beginValidation()
{
    std::cout << "testing\n";
    std::string random_string = "test";
    random_string = auth_server.gen_random(5);

    auth_server.sign(random_string);

    //send file to client
    ifstream toSend("serversignedencoded");
    std::stringstream toSendStream;
    toSendStream << toSend.rdbuf();
    std::string s; int size;
    while(toSendStream >> s )
    {
        size += s.size();
        control_socket->sendFtpMsg(s);
        control_socket->receiveMessage();
    }
    control_socket->sendFtpMsg("end");
    
    std::string check = "";
    std::string recievedMessage = "";
    std::string recievedKey = "";
    control_socket->receiveMessage();
    while (check != "end")
    {
        recievedMessage += check;
        recievedMessage += "\n";
        check = control_socket->receiveMessage();
        control_socket->sendFtpMsg("1");
    } 
    check = "";
    
    bool first = true;
    while (check != "end")
    {
        recievedKey += check;
        recievedKey += "\n";
        check = control_socket->receiveMessage();
        control_socket->sendFtpMsg("1");
    }

   return auth_server.checkValidity(recievedKey, recievedMessage, random_string);
    

}

void Server::disconnect(){
    control_socket->disconnect();
}