#include <iostream>

#include "server.hpp"
// #include "telnet.hpp"
#include "client.hpp"
#include <signal.h>
#include <sys/wait.h>

#include <unistd.h>

int main(int, char **)
{
    std::cout <<"Hello world" << std::endl;
    
    Client mainClient;
    
    std::cout<<"\n Please input path to private key you wish to use to enncode\n";
    std::string pathToPrivKey,pathToPubKey;
    std::getline(std::cin, pathToPrivKey);
    std::cout<<"\nPathToKey: "<<pathToPrivKey<<std::endl<<"--------------\n";
    // ../include/alternative-client-key/client_private_key2.pem
    // ../include/client-keys/client_private_key.pem
    std::cout<<pathToPrivKey.c_str()<<'\n';
    std::cout<<"--------------\n";
    std::cout<<"please type in the path to a matching public key\n";
    // ../include/alternative-client-key/client_public_key2.pem
    // ../include/client-keys/client_public.pem
    std::getline(std::cin, pathToPubKey);
    bool conn_cond = mainClient.connect("0.0.0.0");
    if(!mainClient.validate(pathToPrivKey,pathToPubKey))
    {
        throw("failed to validate server");
    }
    /**
     * @brief necessary as placing these in their proper spots causes segfault
     * 
     */
    mainClient.receiveResponse();
    mainClient.receiveResponse();
    std::pair<int, std::string> response(200, "OK");
    std::string msg;
    response = mainClient.receiveResponse();
    std::cout<<"\n res: "<<response.first<<"\n"<<response.second<<"\n------\n";
    while (conn_cond && response.first != 221 )
    {
        std::cout << "FTP> ";
        std::getline(std::cin, msg);

        mainClient.sendFtpMsg(msg);
        response = mainClient.handleMsg(msg);
        std::cout << "Response: " << response.second << std::endl;
    }
    mainClient.disconnect();
    std::cout << "Exit code: " << response.first << std::endl;
}
