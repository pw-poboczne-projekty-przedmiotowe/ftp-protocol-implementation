#include <iostream>

#include "server.hpp"
// #include "telnet.hpp"
#include <unistd.h>
#include "authorization.hpp"
#include "transmission.hpp"
#include <mutex>
std::mutex auth_mutex;

int main(int, char **)
{
    std::cout << "Hello, world!\n";
    Telnet t(true);
    t.waitForConnect();
    bool authsuccess;
    Server mainServer(&t);
    auth_mutex.lock();
    authsuccess = mainServer.beginValidation();
    auth_mutex.unlock();

    std::string msg = "Init message";

    if(!authsuccess)
    {
        std::cout << "\nACCESS DENIED\n"<< std::endl;
        mainServer.sendFtpResponse(221,"ACCESS DENIED");
        mainServer.disconnect();
        std::cout << "Leaving\n";
    }
    mainServer.sendFtpResponse(200,"ACCESS GRANTED");
    while (msg.length() && msg != "QUIT")
    {
        msg = mainServer.receiveMessage();
        std::cout << "RECEIVE: " << msg << std::endl;
        mainServer.handleMsg(msg);
    }
    mainServer.disconnect();
    std::cout << "Leaving\n";
}
