#include "transmission.hpp"

extern "C"
{
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
}

#include <iostream>
#include <vector>
#include <signal.h>
#include <sstream> 

const auto connect_sock = connect;

DataTransmission::DataTransmission() 
{
}

DataTransmission::~DataTransmission()
{
    close(socket_id);
}

bool DataTransmission::connect(std::string target)
{
    const char *port = std::to_string(DEFAULT_PORT).c_str();
    if (is_connected)
    {
        return false;
    }

    /* get info about target*/
    struct addrinfo *ai;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    int ret_value;
    if ((ret_value = getaddrinfo(target.c_str(), port, &hints, &ai)) != 0)
    {
        fprintf(stderr, "dt getaddrinfo() failed for %s: %s\n", target.c_str(),
                gai_strerror(ret_value));
        return false;
    }

    /* create server socket */
    if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "dt socket() failed: %s\n", strerror(errno));
        return false;
    }

    /* connect socket*/
    if (connect_sock(socket_id, ai->ai_addr, ai->ai_addrlen) == -1)
    {
        fprintf(stderr, "dt connect() failed: %s\n", strerror(errno));
        close(socket_id);
        return false;
    }

    /* free address lookup info */
    freeaddrinfo(ai);

    connected_device = target;
    is_connected = true;

    return true;
}


bool DataTransmission::waitForConnection(float timeout)
{

    if (is_connected)
    {
        return false;
    }



    /*Create listening socket*/
    if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "dt socket() failed: %s\n", strerror(errno));
        return false;
    }


    struct sockaddr_in address;
    inet_aton("127.0.0.1", &address.sin_addr);
    address.sin_family = AF_INET;
    address.sin_port = htons(DEFAULT_PORT);
    
    if (bind(socket_id, (struct sockaddr *)&address, sizeof(address)) == -1)
    {
        fprintf(stderr, "dt bind() failed: %s\n", strerror(errno));
        fprintf(stderr, "For some lower ports sudo may be needed\n");
        close(socket_id);
        return false;
    }



    /*wait for server to connect*/
    if (listen(socket_id, 1) == -1)
    {
        fprintf(stderr, "dt listen() failed: %s\n", strerror(errno));
        close(socket_id);
        return false;
    }



    socklen_t addrlen = sizeof(address);
    int tmp_socket;
    if ((tmp_socket = accept(socket_id, (struct sockaddr *)&address,
                                &addrlen)) == -1)
    {
        fprintf(stderr, "dt accept() failed: %s\n", strerror(errno));
        close(socket_id);
        return false;
    }

    socket_id = tmp_socket;
    is_connected = true;

    
}

void DataTransmission::abortTransmission()
{
    shutdown(socket_id,SHUT_RDWR);
    is_connected = false;
    close(socket_id);
       
    connected_device = "";
    data_received = "";
}

void DataTransmission::receiveData(bool cond)
{
    char buffer[256] = {0};
    int valread = 256;
    std::stringstream stream;

    data_received.clear();
    int frames = 0;

    while (valread == 256)
    {
        valread = recv( socket_id , buffer, sizeof(buffer), 0);
        data_received.append(buffer);
        buffer[256] = {0};
        frames++;
    }
    int to_delete = 256 - valread;


    if(valread < 0)
    {
        is_data_received = false;
        fprintf(stderr, "dt receiving data failed: %s\n", strerror(errno));
    }
    else{
        is_data_received = true;
    }

    if (frames > 1)
    {
        for(int i = 0; i < to_delete; i++)
        {
            data_received.pop_back();
        }
    }
};

void DataTransmission::sendData(std::string message, bool cond)
{   
    is_transmitting = true;
    int n;
    if(n = send(socket_id, message.c_str(), message.size(), 0) ==-1)
        fprintf(stderr, "dt failed to send data: %s\n", strerror(errno));
    is_transmitting = false;

}