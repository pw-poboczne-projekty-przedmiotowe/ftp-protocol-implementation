#include "client.hpp"

#include <iostream>
#include <filesystem>
#include <fstream> 
#include <streambuf>
#include <iostream>

Client::Client(/* args */)
{
}

Client::~Client()
{
}


bool Client::connect(std::string address)
{
    return control_socket.connect(address);
}

bool Client::sendFtpMsg(std::string msg)
{
    return control_socket.sendFtpMsg(msg);
}

std::pair<int, std::string> Client::receiveResponse()
{
    return control_socket.receiveResponse();
}

bool Client::validate(std::string pathToPrivKey, std::string pathToPubKey)
{
    //std::string testing_args = "../include/alternative-client-key/client_public_key2.pem";

    std::string recieved; 
    std::string test = "";
    while (test != "end")
    {
        recieved += test;
        recieved += "\n";
        test = control_socket.receiveMessage();
        control_socket.sendFtpMsg("1");
    }
    ofstream fileRecieved("clientrecieved");
    if(!fileRecieved.is_open())
    {
        throw("Error - could not open file to write\n");
        return false;
    }
    fileRecieved << recieved;
    fileRecieved.close();
    if(!auth_client.checkValidity("t"))
    {
        std::cout<<"Failed to authenticate server"<<std::endl;
        return false;
    }

    auth_client.hashPassword(pathToPrivKey);
    ifstream fileMessageToSend("messageEncodedByClient");
    if(!fileMessageToSend.is_open())
    {
        throw("Error - could not open file with message encoded by client\n");
        return false;
    }
    //"../include/alternative-client-key/client_public_key2.pem"
    ifstream fileKeyToSend(pathToPubKey.c_str());
    if(!fileKeyToSend.is_open())
    {
        throw("Error - could not open file with key to send by client\n");
        return false;
    }
    std::stringstream streamMessageToSend;
    std::stringstream streamKeyToSend;
    streamMessageToSend << fileMessageToSend.rdbuf();
    streamKeyToSend << fileKeyToSend.rdbuf();
    std::string s;
    while(streamMessageToSend >> s )
    {
        control_socket.sendFtpMsg(s);
        control_socket.receiveMessage();
    }
    control_socket.sendFtpMsg("end");
    //control_socket.receiveResponse();
    //std::string seg_fault;
    while(streamKeyToSend >> s )
    {
        control_socket.sendFtpMsg(s);
        control_socket.receiveMessage();
    }
    control_socket.sendFtpMsg("end");
    //control_socket.receiveResponse();
    return true;
}

std::pair<int, std::string> Client::handleMsg(std::string message)
{
    char *token = strtok(&message[0], " ");
    std::pair<int, std::string>  response;

    if (strcmp("MKDIR", token) == 0)
    {
        return receiveResponse();
    }
    else if (strcmp("RMDIR", token) == 0)
    {
        return receiveResponse();
    }
    else if (strcmp("CHANGE", token) == 0)
    {
        return receiveResponse();

    } else if(strcmp("LIST", token) == 0){
        response = receiveResponse();
        if (response.first == 200)
            return receiveResponse();
        return response; 

    } else if(strcmp("STORE", token) == 0){
        response = receiveResponse();
        if (response.first == 200)
        {

            token=strtok(NULL," \n");

            data_transmittion.waitForConnection();
            std::fstream fs;
            fs.open (token);
            if(!fs)
                std::cout << "Cannot load a file\n";
            std::stringstream buffer;
            buffer << fs.rdbuf();
            fs.close();
            data_transmittion.sendData(buffer.str(), true);

        }
        return response;

    } else if(strcmp("RETRIEVE", token) == 0){
        response = receiveResponse();
        if (response.first == 200)
        {
            token=strtok(NULL," \n");
            std::string str = token;
            std::string temp;

            for(char &c : str)
            {
                if(c == '/')
                {
                    temp.clear();
                }
                else
                    temp += c;
            }

            data_transmittion.waitForConnection();
            data_transmittion.receiveData(true);
            std::ofstream file(temp);
            if(file.is_open())
            {
                file << data_transmittion.getData();
                file.close();
            }
            
        }
            
        return response;

    } else if(strcmp("QUIT", token) == 0){
        return receiveResponse();
    }
    else if (strcmp("QUIT", token) == 0)
    {
        return receiveResponse();
    }
    else
    {
        return receiveResponse();
    }
}

void Client::disconnect()
{
    control_socket.disconnect();
}
