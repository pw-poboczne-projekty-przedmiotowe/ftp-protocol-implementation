#include "telnet.hpp"

extern "C"
{
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
}

const auto connect_sock = connect;

const telnet_telopt_t Telnet::telopts[] = {
    {TELNET_TELOPT_ECHO, TELNET_WONT, TELNET_DO},
    {TELNET_TELOPT_TTYPE, TELNET_WILL, TELNET_DONT},
    {TELNET_TELOPT_COMPRESS2, TELNET_WONT, TELNET_DO},
    {TELNET_TELOPT_MSSP, TELNET_WONT, TELNET_DO},
    {-1, 0, 0}};

Telnet::Telnet(bool debug) : debug(debug), connected_device("")
{
    messages.push_back("");
    messages.clear();
}

Telnet::~Telnet()
{
    if (is_connected)
    {
        disconnect();
    }
}

void Telnet::event_handler(telnet_t *telnet, telnet_event_t *ev, void *user_data)
{
    Telnet *tel_ptr = (Telnet *)user_data;
    tel_ptr->log_debug("TELNET handler called with event type: %d\n", ev->type);
    int socket = tel_ptr->socket_id;

    ssize_t ret;
    switch (ev->type)
    {
    case TELNET_EV_SEND:
        ret = send(socket, ev->data.buffer, ev->data.size, 0);
        if (ret == -1)
        {
            tel_ptr->log_debug("Got -1 after sending, closing connection");
            tel_ptr->disconnect();
        }
        else if (ret != ev->data.size)
        {
            tel_ptr->log_debug("Got %d after sending %d bytes", ret, ev->data.size);
        }
        break;
    case TELNET_EV_DATA:
        tel_ptr->addNewMessage(ev->data.buffer, ev->data.size);
        break;
    default:
        fprintf(stderr, "Received unexpected type of message: %d\n", ev->type);
        break;
    }
};

void Telnet::addNewMessage(const char *buffer, size_t size)
{
    messages.push_back(std::string(buffer, size));
}

void Telnet::log_debug(const char *msg, ...)
{
    if (debug)
    {
        va_list args;
        va_start(args, msg);
        vprintf(msg, args);
        va_end(args);
    }
}

bool Telnet::connect(std::string target)
{
    const char *port = std::to_string(DEFAULT_PORT).c_str();
    if (is_connected)
    {
        return false;
    }

    /* get info about target*/
    struct addrinfo *ai;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    int ret_value;
    if ((ret_value = getaddrinfo(target.c_str(), port, &hints, &ai)) != 0)
    {
        fprintf(stderr, "getaddrinfo() failed for %s: %s\n", target.c_str(),
                gai_strerror(ret_value));
        return false;
    }

    /* create server socket */
    if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "socket() failed: %s\n", strerror(errno));
        return false;
    }

    /* connect socket*/
    if (connect_sock(socket_id, ai->ai_addr, ai->ai_addrlen) == -1)
    {
        fprintf(stderr, "connect() failed: %s\n", strerror(errno));
        close(socket_id);
        return false;
    }

    /* free address lookup info */
    freeaddrinfo(ai);

    log_debug("TELNET Connected successfully\n");
    connected_device = target;
    is_connected = true;

    telnet_connection = telnet_init(telopts, Telnet::event_handler, 0, this);

    return true;
}

bool Telnet::disconnect()
{
    if (!is_connected)
        return true;

    telnet_free(telnet_connection);
    telnet_connection = nullptr;
    is_connected = false;
    close(socket_id);
    connected_device = "";

    return true;
}

bool Telnet::waitForConnect()
{
    if (is_connected)
    {
        return false;
    }

    /*Create listening socket*/
    if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "socket() failed: %s\n", strerror(errno));
        return false;
    }

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_port = htons(DEFAULT_PORT);
    pid_t child_pid;

    if (bind(socket_id, (struct sockaddr *)&address, sizeof(address)) == -1)
    {
        fprintf(stderr, "bind() failed: %s\n", strerror(errno));
        fprintf(stderr, "For some lower ports sudo may be needed\n");
        close(socket_id);
        return false;
    }

    log_debug("WAIT FOR CLIENT\n");
    /* wait for client */
    if (listen(socket_id, 1) == -1)
    {
        fprintf(stderr, "listen() failed: %s\n", strerror(errno));
        close(socket_id);
        return false;
    }

    while(true){

        socklen_t addrlen = sizeof(address);
        int tmp_socket;
        if ((tmp_socket = accept(socket_id, (struct sockaddr *)&address,
                                &addrlen)) == -1)
        {
            fprintf(stderr, "accept() failed: %s\n", strerror(errno));
            close(socket_id);
            return false;
        }

        if((child_pid = fork()) == 0){
            //child process
            char *str = new char[INET_ADDRSTRLEN];
            connected_device = inet_ntop(AF_INET, &(address.sin_addr), str, INET_ADDRSTRLEN);
            log_debug("CHILD CONNECTION RECEIVED addr: %s\n", str);
            delete (str);

            close(socket_id);
            socket_id = tmp_socket;

            telnet_connection = telnet_init(0, Telnet::event_handler, TELNET_FLAG_PROXY, this);
            is_connected = true;
            return true;
        } else {
            //parent process
            close(tmp_socket);
        }
    }
}

std::string Telnet::receiveMessage()
{
    if (!is_connected)
        return "";

    int ret = 0;
    char buffer[MAXLINE];

    log_debug("Wait for message\n");
    ret = recv(socket_id, buffer, MAXLINE, 0);
    if (ret <= 0)
    {
        fprintf(stderr, "recv() returned %d, closing connection\n", ret);
        disconnect();
        return "";
    }
    log_debug("Got message %d\n", ret);
    telnet_recv(telnet_connection, buffer, ret);

    std::string result = *messages.begin();
    messages.pop_front();

    return result;
};

void Telnet::sendFtpResponse(int code, std::string message)
{
    std::string target_msg = std::to_string(code);
    if (message.length())
        target_msg += " " +
                      message;
    send_text(target_msg);
}

bool Telnet::send_text(std::string message)
{
    telnet_send_text(telnet_connection, message.c_str(), message.length());
    return is_connected;
}

// std::pair<int, std::string> Telnet:: sendFtpMsg(std::string message)
// {
//     send_text(message);
//     std::string response = receiveMessage();
//     if (response.length() == 0)
//     {
//         disconnect();
//         return std::pair<int, std::string>(421, "Service not available, closing control connection.");
//     }

//     int retnum = std::stoi(response.substr(0, 3));
//     std::string ret_msg = "";
//     if (response.length() > 3)
//         ret_msg = response.substr(4);

//     return std::pair<int, std::string>(retnum, ret_msg);
// }

bool Telnet::sendFtpMsg(std::string message)
{
    return send_text(message);
}

std::pair<int, std::string> Telnet::receiveResponse(){
    std::string response = receiveMessage();
    if (response.length() == 0)
    {
        disconnect();
        return std::pair<int, std::string>(421, "Service not available, closing control connection.");
    }

    int retnum = std::stoi(response.substr(0, 3));
    std::string ret_msg = "";
    if (response.length() > 3)
        ret_msg = response.substr(4);

    return std::pair<int, std::string>(retnum, ret_msg);
}
