#include "authorization.hpp"

// additional-key-tests are client keys
AuthorizationClient::AuthorizationClient(/* args */)
{
}

AuthorizationClient::~AuthorizationClient()
{

}



void AuthorizationClient::hashPassword(std::string pathToPrivKey)
{
    std::string scriptpath = "../include/scripts/clientEncode.sh ";
    scriptpath += pathToPrivKey;
    system(scriptpath.c_str());
}

bool AuthorizationClient::checkValidity(std::string str)
{

    system("../include/scripts/clientDecode.sh");
    ifstream decodedMessageFile("messageDecodedByClient");
    if(!decodedMessageFile.is_open())
    {
        std::cout<<"failed to open decoded message\n";
        return false;
    }

    std::stringstream decodedMessageStream;
    decodedMessageStream << decodedMessageFile.rdbuf();
    std::string decodedMessage = decodedMessageStream.str();
    if(decodedMessage == "")
    {
        std::cout<<"empty message\n";
        return false;
    }
    decodedMessageFile.close();
    return true;
}

AuthorizationServer::AuthorizationServer(/* args */)
{

}

std::string AuthorizationServer::gen_random(const int len)
{

    std::string tmp_s;
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    srand((unsigned)time(NULL) * getpid());

    tmp_s.reserve(len);

    for (int i = 0; i < len; ++i)
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];

    return tmp_s;
}
AuthorizationServer::~AuthorizationServer()
{
}

bool AuthorizationServer::checkValidity(std::string recievedKey, std::string recievedMessage, std::string originalMessage)
{
    const std::filesystem::path pathToShow = "../include/accepted-keys-server";

    for (const auto &entry : std::filesystem::directory_iterator(pathToShow))
    {
        const auto filenameStr = entry.path().filename().string();
        if (entry.is_directory())
        {
            std::cout << "dir:  " << filenameStr << '\n';
        }
        else if (entry.is_regular_file())
        {
            std::string pathToKey = "";
            pathToKey += pathToShow.c_str();
            pathToKey += "/";
            pathToKey += filenameStr;
            ifstream KeyFile(pathToKey.c_str());
            std::stringstream KeyBuffer;
            KeyBuffer << KeyFile.rdbuf();
            std::string tmp = "";
            std::string key = "";
            bool first = true; key = KeyBuffer.str();

            
            std::cout<<"Checking key : "<<pathToShow.string()<<'/'<<filenameStr<<"\n";
            key.erase(remove(key.begin(), key.end(), '\n'), key.end()); 
            key.erase(remove(key.begin(), key.end(), ' '), key.end()); 

            recievedKey.erase(remove(recievedKey.begin(), recievedKey.end(), '\n'), recievedKey.end()); 
            
            if (key == recievedKey)
            {
                ofstream filemessageRecieved("recievedByServer");
                filemessageRecieved << recievedMessage;
                filemessageRecieved.close();
                std::string decipherScript = "../include/scripts/tryDecrypt.sh ";
                decipherScript += filenameStr;
                system(decipherScript.c_str());
                ifstream verifiedMessageFile("verifiedByServer");
                std::stringstream verifiedMessageBuffer;
                verifiedMessageBuffer << verifiedMessageFile.rdbuf();
                std::string verifiedMessage = verifiedMessageBuffer.str();

                if (verifiedMessage == originalMessage)
                {
                    std::cout<<"Verified message correct\n";
                    return true;
                }
                else
                {
                    std::cout<<"Verified message incorrect\n";
                    return false;
                }
            }
        }
        else
            std::cout << "??    " << filenameStr << '\n';
    }
    std::cout<<"did not find a matching key\n";
    return false;

}

void AuthorizationServer::sign(std::string message)
{
    std::string filename = message;
    ofstream MyFile("test");
    MyFile << message;
    MyFile.close();
    std::string cipher = "../include/scripts/serverSign.sh";
    system(cipher.c_str());
}
