# FTP-Protocol-implementation

Basic implementation of FTP protocol based on RFC 959
https://tools.ietf.org/html/rfc959


# Build instruction

```bash
sudo apt install cmake 

mkdir build 
cd build
cmake ..
make
```

# User instruction

The scripts responsible for authorisation are context-dependent, so the apps need to be launched from the /build folder.
The keys must be in the .pem format and must have matching accepted keys - see included examples.
The code for ftp-client-main contains 2 sets of paths to keys, these are the keys in the client-keys forlder and alternate-client-key folder.
If using them to authenthicate make sure to use matching ones, otherwise the authentification will fail (ACCESS DENIED)

# System requirements

The app works on Linux systems and requires OpenSSL to be installed.

# Example scenario

1) launch ftp-server
2) launch ftp-client
3) paste the first proposed key (../include/alternative-client-key/client_private_key2.pem) 
4) paste matching public key (../include/alternative-client-key/client_public_key2.pem)
5) paste 'LIST' command
6) see the list of existing files
7) type 'MKDIR example' to create a directory named example
8) type 'CHANGE example' to enter the created directory
9) type 'STORE <path-to-file>' where path-to-file is a path to a text file you wish to STORE
10) type 'LIST' to see wether the file exists
11) type 'QUIT' to exit
