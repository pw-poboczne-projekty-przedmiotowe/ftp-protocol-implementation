#!/bin/bash
#openssl smime -encrypt -in /home/msobiera/tin/ftp-protocol-implementation/client-keys/text.txt -out /home/msobiera/tin/ftp-protocol-implementation/client-keys/encryptedtest.txt /home/msobiera/tin/ftp-protocol-implementation/client-keys/publickey.cer
cd /home/msobiera/tin/ftp-protocol-implementation/include/additional-key-tests
openssl rsautl -encrypt -inkey public.pem -pubin -in tests.txt -out tests_encr.txt
penssl rsautl -decrypt -inkey private_key.pem -in tests_encr.txt -out tests_decr.txt