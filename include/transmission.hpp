#pragma once

#include <string>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <utility>
#include <list>

class DataTransmission
{
private:
    //Connection-related
    bool is_connected = false;
    int socket_id;
    int server, client;

    static const int DEFAULT_PORT = 2223;

    //Others
    std::string connected_device;
    bool is_transmitting = false;
    bool is_data_received = false;
    pid_t child_pid_temp = -1;

    //Data
    std::string data_received;


public:
    DataTransmission();
    ~DataTransmission();

    /**
     * @brief initialize connection with selected IP 
     * 
     * @param IP
     */
    bool connect(std::string target);

    /**
     * @brief start waiting for a connection this is blocking method
     * 
     */
    bool waitForConnection(float timeout = 1);

    /**
     * @brief collect received data as ASCII string for further manipulation
     * 
     */
    std::string getData() { return data_received; }

    /**
     * @brief check wether data was successfully received
     * 
     */
    bool isDataReceived() { return is_data_received; }

    /**
     * @brief create a new thread for storing incoming data
     * 
     */
    void receiveData(bool cond = false);

    /**
     * @brief 
     * 
     * @param data 
     */
    void sendData(std::string data, bool cond = false);

    /**
     * @brief 
     */
    bool isTransmitting() { return is_transmitting; }
    /**
     * @brief stop receiving and sending data
     * 
     */
    void abortTransmission();
};
