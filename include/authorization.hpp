#pragma once

#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <random>
#include <ctime>
#include <unistd.h>
#include <sstream>
#include <filesystem>


using namespace std;

class AuthorizationClient
{
private:
    /* data */
public:
    AuthorizationClient(/* args */);
    ~AuthorizationClient();
    /**
     * @brief ensures the message recieved from server exists and can be decoded into readable format
     * 
     * @param recieved message recieved from server
     * @return true if the message exists and can be decoded
     * @return false if the message does not exist or cannot be decoded
     */
    bool checkValidity(std::string recieved);
    /**
     * @brief hashes the message recieved from server
     * 
     * @param pathToPrivKey pat to private key used for hasing
     * 
     */
    void hashPassword(std::string pathToPrivKey);
    
};

class AuthorizationServer
{
private:
    /* data */
public:
    AuthorizationServer(/* args */);
    
    /**
     * @brief generates random string of desired length
     * 
     * @param len length of string to be generated
     * @return std::string random string
     */

    std::string gen_random(const int len);
    ~AuthorizationServer();
    /**
     * @brief checks the validity of client
     * 
     * @param recievedKey key the client sent
     * @param recievedMessage message the client sent signed
     * @param originalMessage original message the server sent to clinet
     * @return true if the messages match after successfull decoding
     * @return false if the messages don't match after successfull decoding
     */

    bool checkValidity(std::string recievedKey, std::string recievedMessage, std::string originalMessage);
    /**
     * @brief server signs and hashes a random message
     * 
     * @param message message to be signed
     */
    
    void sign(std::string message);
};

