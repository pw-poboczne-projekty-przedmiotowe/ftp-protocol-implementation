#pragma once

#include <string>
#include <utility>
#include <list>

extern "C"
{
#include "libtelnet.h"
}

#define MAXLINE 1024 

class Telnet
{
private:
    //Connection-related
    bool is_connected = false;
    int socket_id;
    telnet_t *telnet_connection;

    static const int DEFAULT_PORT = 2222;
    static const telnet_telopt_t telopts[];

    // Data-related
    std::list<std::string> messages;

    //Others
    bool debug;
    std::string connected_device;

    //Private Methods
    static void event_handler(telnet_t *telnet, telnet_event_t *ev, void *user_data);
    void addNewMessage(const char *buffer, size_t size);
    void log_debug(const char *msg, ...);
    bool send_text(std::string message);

public:
    Telnet(bool debug = false);
    ~Telnet();

    /**
     * @brief Initialize connection with target
     * 
     * @param target - Target IP
     * @return true - connected successfully
     */
    bool connect(std::string target);
    bool disconnect();
    /**
     * @brief Wait from external connection.
     *        This is blocking function.
     * 
     * @return true - success
     */
    bool waitForConnect();

    /**
     * @brief 
     * 
     * @param message 
     * @return int - return code of FTP response,  string- returned message
     */
    bool sendFtpMsg(std::string message);

    /**
    * @brief this is blocking function used for obtaining message from telnet
    * 
    * @return message, empty, when disconnected gracefully 
    * @throws std::exception when disconnected without warning
    */
    std::string receiveMessage();

    /**
     * @brief function meant for server. It sends ftp responses to client
     * 
     * @param code - return code according to RFC959
     * @param message - string describing message
     */
    void sendFtpResponse(int code, std::string message = "");
    std::string getConnectedIP() { return connected_device; };

    std::string handleMsgClient(std::string message);
    std::pair<int, std::string> receiveResponse();
};
