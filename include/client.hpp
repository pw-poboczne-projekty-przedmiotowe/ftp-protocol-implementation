#pragma once
#include "telnet.hpp"
#include "transmission.hpp"

extern "C"
{
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
}

#include "authorization.hpp"
class Client
{
private:
    Telnet control_socket = Telnet(true);
    DataTransmission data_transmittion;
    pid_t child_pid = -1;
    AuthorizationClient auth_client;

public:
    Client(/* args */);
    ~Client();
    bool connect(std::string address);
    bool sendFtpMsg(std::string);
    std::pair<int, std::string> receiveResponse();
    std::pair<int, std::string> handleMsg(std::string message);
    void disconnect();
    bool validate(std::string pathToPrivKey, std::string pathToPubKey);
};

