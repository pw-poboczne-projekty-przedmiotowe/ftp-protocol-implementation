#pragma once

#include "telnet.hpp"
#include "transmission.hpp"
#include "authorization.hpp"


extern "C"
{
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
}

class Server
{
private:
    Telnet* control_socket;
    DataTransmission data_transmittion;
    AuthorizationServer auth_server;
public:
    Server(Telnet *control);
    ~Server();

    void waitForConnect();
    void disconnect();
    std::string receiveMessage();
    void handleMsg(std::string message);
    bool beginValidation();
    void  sendFtpResponse(int code, std::string message);
};